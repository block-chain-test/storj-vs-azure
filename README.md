# Storj vs Azure

This project will test a Storj.io-storage service and the Microsoft Azure-storage service.
These will then be compared with eachother

## Getting Started

The instructions for the usage of this test software is below.

### Prerequisites

To make the tests work, the following files/directories have to be created:
* **./constants.js** - Should contain connection string (Azure) and credentials (Storj.io) as follows:

```
//Connection string to Azure Blob service
exports.connectionStringEU = "---"
exports.connectionStringUS = "---"

//Credentials for Storj.io access
exports.username = "---"
exports.password = "---"
exports.bucketId = "---"
```

* **./downloads/azure** - All files downloaded from Azure will be placed here
* **./downloads/storj** - All files downloaded from Storj.io will be placed here
* **./data** - The .xlsx files will be placed here containing the times

### Installing

All modules needed for this software are included in package.json.
Run the following command to install them:

```
npm install
```

## Running the tests

There are six tests. How to start them and what they do:

Uploads and downloads a big file 50 times to/from Azure (EU-West):
```
node index 1
```
Uploads and downloads a big file 50 times to/from Storj.io:
```
node index 2
```
Uploads and downloads a small file 50 times to/from Azure (EU-West):
```
node index 3
```
Uploads and downloads a small file 50 times to/from Storj.io:
```
node index 4
```
Uploads and downloads a big file 50 times to/from Azure (US-West):
```
node index 5
```
Uploads and downloads a small file 50 times to/from Azure (US-West):
```
node index 6
```
