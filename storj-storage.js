"use strict";

const {Environment} = require('storj');
const CONSTANTS = require('./constants.js');
const randomstring = require("randomstring");
const bucketId = CONSTANTS.bucketId;
const fileIdentifier = 'Romil';
const numberOfTests = 30;
var fileUpload = ""
var fileDownload = ""
var fileIdArr = [] //Will hold all the unique identifiers to the files

const storj = new Environment({
  bridgeUrl: 'https://api.storj.io',
  bridgeUser: CONSTANTS.username,
  bridgePass: CONSTANTS.password,
  encryptionKey: 'abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon about'
});

//Uploads and downloads one big file 100 times to/from Storj.io
module.exports.startTestStorj = function(fileToUpload, number, fileToDownload, callback){
  fileUpload = fileToUpload
  fileDownload = fileToDownload
  var payload = {}

  upload(number, function(uploadPayload){
    if(uploadPayload.error){
      callback(uploadPayload)
    }else{
      download(number, uploadPayload.fileId, function(downloadPayload){
        if(downloadPayload.error){
          downloadPayload.message = "Test "+number+" failed!"
        }
        payload.uploadRes = uploadPayload
        payload.downloadRes = downloadPayload

        callback(payload)
      })
    }
  })

  // uploadMultipleFiles(function(payloadUpload){
  //   if(payloadUpload.error){  //An error occurred
  //     callback(payloadUpload)
  //   }
  //   payload.uploadTimeArr = payloadUpload.uploadTimeArr
  //
  //   downloadMultipleFiles(function(payloadDownload){
  //     if(payloadDownload.error){ //An error occurred
  //       callback(payloadDownload)
  //     }
  //     payload.downloadTimeArr = payloadDownload.downloadTimeArr
  //     payload.cloud = "storj"
  //     callback(payload) //callback with the final times
  //   })
  // })
}

/**
 * Uploads the file specified by the fileUpload variable found in the fileIdArr
 * number of times specified in the numberOfTests constant
 *
 * @param {integer} callback
 *   Callback that gets called when this function is done executing.
 */
// function uploadMultipleFiles(callback){
//   var uploadTimeArr = []
//   for (var i = 0; i < numberOfTests; i++) {
//     upload(i, function(payload){
//       if(payload.error){
//         callback(payload)
//       }
//
//       uploadTimeArr.push(payload.time)
//       fileIdArr.push(payload.fileId)
//       if(uploadTimeArr.length == numberOfTests){
//         payload.uploadTimeArr = uploadTimeArr
//         callback(payload)
//       }
//     })
//   }
// }

/**
 * Downloads all the files found in the fileIdArr.
 *
 * @param {callback} callback
 *   Callback that gets called when this function is done executing.
 */
// function downloadMultipleFiles(callback){
//   var downloadTimeArr = []
//   for (var i = 0; i < numberOfTests; i++) {
//     download(i, fileIdArr[i], function(payload){
//       if(payload.error){
//         callback(payload)
//       }
//
//       downloadTimeArr.push(payload.time)
//       if(downloadTimeArr.length == numberOfTests){
//         payload.downloadTimeArr = downloadTimeArr
//         callback(payload)
//       }
//     })
//   }
// }

/**
 * Uploads the file pre-defined in the fileUpload constant.
 *
 * @param {integer} number
 *   Specifies the current number of the file to be uploaded.
 *   This gets concatinated to the file/blob name (i.e. filename1, filename2, filename3,...,filenameN)
 * @param {integer} callback
 *   Callback that gets called when this function is done executing.
 */

 function upload(number, callback){
  var payload = {}
  var randomString = randomstring.generate(12);
  payload.randomString = randomString
  var time = ""
  var begin = process.hrtime()
  storj.storeFile(CONSTANTS.bucketId, fileUpload, {
    filename: fileIdentifier+randomString,
    progressCallback: function(progress,downloadedBytes,totalBytes){
    },
    finishedCallback: function(error, fileId){
      time = process.hrtime(begin)
      if(error){
        payload.error = error
      }
      payload.fileId = fileId
      payload.time = time[0] + time[1]/1000000000
      callback(payload)
    }
  })
}

/**
 * Downloads the file pre-defined in the fileUpload constant.
 *
 * @param {integer} number
 *   Specifies the current number of the file to be uploaded.
 *   This gets concatinated to the file/blob name (i.e. filename1, filename2, filename3,...,filenameN)
 * @param {integer} callback
 *   Callback that gets called when this function is done executing.
 */
 function download(number, fileId, callback){
   var payload = {}
   payload.fileId = fileId
   var time = ""
   var begin = process.hrtime()
   storj.resolveFile(bucketId, fileId, fileDownload+fileId, {
     progressCallback: function(progress, downloadedBytes, totalBytes){
     },
     finishedCallback: function(error){
       time = process.hrtime(begin)
       if(error){
         payload.error = error
       }
       payload.fileId = fileId
       payload.time = time[0] + time[1]/1000000000
       callback(payload)
     }
   })
 }
