"use strict";

const CONSTANTS = require("./constants.js")
const CONTAINER = "mycontainer" //The container that holds the file(s)
const azure = require("azure-storage")
var blobSvc = azure.createBlobService(CONSTANTS.connectionStringEU) //Connectionstring to EU-West
const numberOfTests = 30
var fileUpload = ""
var fileDownload = ""

//Uploads and downloads one big file to/from Azure (EU-West)
module.exports.startTestAzure = function(fileToUpload, number, fileToDownload, region, callback){
  fileUpload = fileToUpload
  fileDownload = fileToDownload
  var payload = {}

  if(region == "US"){
    blobSvc = azure.createBlobService(CONSTANTS.connectionStringUS)
  }
  payload.cloud = "azure"
  payload.region = region

  upload(number, function(uploadPayload){
    if(uploadPayload.error){
      callback(uploadPayload)
    }else{
      download(number, function(downloadPayload){
        if(downloadPayload.error){
          downloadPayload.message = "Test "+number+" failed!"
        }
        payload.uploadRes = uploadPayload
        payload.downloadRes = downloadPayload

        callback(payload)
      })
    }
  })

  // uploadMultipleFiles(function(payloadUpload){
  //   if(payloadUpload.error){
  //     callback(payloadUpload)
  //   }
  //   payload.uploadTimeArr = payloadUpload.uploadTimeArr
  //
  //   downloadMultipleFiles(function(payloadDownload){
  //     if(payloadDownload.error){
  //       callback(payloadDownload)
  //     }
  //     payload.downloadTimeArr = payloadDownload.downloadTimeArr
  //     callback(payload) //callback with the final times
  //   })
  // })
}

// /**
//  * Uploads the file specified by the fileUpload variable found in the fileIdArr
//  * number of times specified in the numberOfTests constant
//  *
//  * @param {callback} callback
//  *   Callback that gets called when this function is done executing.
//  */
// function uploadMultipleFiles(callback){
//   var uploadTimeArr = []
//   for (var i = 0; i < numberOfTests; i++) {
//     upload(i, function(payload){
//       if(payload.error){
//         callback(payload)
//       }
//
//       uploadTimeArr.push(payload.time)
//       if(uploadTimeArr.length == numberOfTests){
//         payload.uploadTimeArr = uploadTimeArr
//         callback(payload)
//       }
//     })
//   }
// }

/**
 * Downloads all the files in the container
 *
 * @param {callback} callback
 *   Callback that gets called when this function is done executing.
 */
// function downloadMultipleFiles(callback){
//   var downloadTimeArr = []
//   for (var i = 0; i < numberOfTests; i++) {
//     download(i, function(payload){
//       if(payload.error){
//         callback(payload)
//       }
//       downloadTimeArr.push(payload.time)
//       if(downloadTimeArr.length == numberOfTests){
//         payload.downloadTimeArr = downloadTimeArr
//         callback(payload)
//       }
//     })
//   }
// }

/**
 * Uploads the file pre-defined in the fileToUpload constant.
 *
 * @param {integer} number
 *   Specifies the current number of the file to be uploaded.
 *   This gets concatinated to the file/blob name (i.e. filename1, filename2, filename3,...,filenameN)
 * @callback callback
 *   Callback that gets called when this function is done executing.
 */
function upload(number, callback){
  var payload = {}
  payload.number = number
  var time = ""
  var begin = process.hrtime()
  blobSvc.createBlockBlobFromLocalFile(CONTAINER, 'myblobAzure'+number, fileUpload, function(error, result, response){
    //Error occurred
    time = process.hrtime(begin)
    if(error){
      //File uploaded
      payload.error = error
    }
    payload.result = result
    payload.response = response
    payload.uploadTime = time[0] + time[1]/1000000000
    //Callback gets called with the created payload
    callback(payload)
  })
}

/**
 * Downloads the file pre-defined in the fileToUpload constant.
 *
 * @param {integer} number
 *   Specifies the current number of the file to be uploaded.
 *   This gets concatinated to the file/blob name (i.e. filename1, filename2, filename3,...,filenameN)
 * @callback callback
 *   Callback that gets called when this function is done executing.
 */
 function download(number, callback){
   var payload = {}
   var fileName = fileDownload+"downloaded"+number
   payload.number = number
   var time = ""
   var begin = process.hrtime()
   blobSvc.getBlobToLocalFile(CONTAINER, "myblobAzure"+number, fileName,
     function(error, result, response){
       time = process.hrtime(begin)
       if(!error){
         //File uploaded
         payload.result = result
         payload.downloadedFile = fileName
       }else{
         //Error occurred
         payload.error = error
       }
       payload.response = response
       payload.downloadTime = time[0] + time[1]/1000000000
       //Callback gets called with the created payload
       callback(payload)
    })
 }
