const validator = require("validator")
const consoleTable = require("console.table")
const xl = require('excel4node');
const commandArguments = process.argv.slice(2)
const bigTestFile = "./test/testimage.jpg"
const smallTestFile = "./test/test.txt"
const downloadDirAzure = "./downloads/azure/"
const downloadDirStorj = "./downloads/storj/"

const azureStorageTests = new require("./azure-storage")
const storjStorageTests = require("./storj-storage")

var numberOfTests = 0

if(commandArguments.length == 0){
  console.log("Invalid parameters. Expected 'node index (test number between 1-6)'")
  return
}

var cloudTest = commandArguments[0]

if(validator.isAlpha(cloudTest)){
  if(cloudTest == "help"){
    showHelpText()
    return
  }else{
    console.log("Invalid parameters. Expected 'node index (test number between 1-6)'")
    return
  }
}

if(cloudTest < 1 || 6 < cloudTest){
  console.log("Invalid parameters should be a number between 1-6")
  return
}

switch (parseInt(cloudTest)) {
  case 1:
    //Uploads and downloads a big file 50 times to/from Azure (EU-West)
    azureStorageTests.startTestAzure(bigTestFile, 1, downloadDirAzure, "EU", handleResults)
    break
  case 2:
    //Uploads and downloads a big file 50 times to/from Storj.io
    storjStorageTests.startTestStorj(bigTestFile, 2, downloadDirStorj, handleResults)
    break
  case 3:
    //Uploads and downloads a small file 50 times to/from Azure (EU-West)
    azureStorageTests.startTestAzure(smallTestFile, 3, downloadDirAzure, "EU", handleResults)
    break
  case 4:
    //Uploads and downloads a small file 50 times to/from Storj.io
    storjStorageTests.startTestStorj(smallTestFile, 4, downloadDirStorj, handleResults)
    break
  case 5:
    //Uploads and downloads a big file 50 times to/from Azure (US-West)
    azureStorageTests.startTestAzure(bigTestFile, 5, downloadDirAzure, "US", handleResults)
    break
  case 6:
    //Uploads and downloads a small file 50 times to/from Azure (US-West)
    azureStorageTests.startTestAzure(smallTestFile, 6, downloadDirAzure, "US", handleResults)
    break
  default:
    console.log("Something went wrong")
}

/**
 * Function that handles and prints out results
 *
 * @param {JSON-Object} payload
 *  payload-object with all data
 */
function handleResults(payload){
  if(payload.error){
    console.log("AN ERROR OCCURRED")
    console.log(payload.error)
  }else{
    //createXLSX(payload)
    console.log(payload)
  }
}

// /**
//  * Function that creates an XLXS-file with all the data
//  * in the payload
//  *
//  * @param {JSON-Object} payload
//  *  payload-object with all data
//  */
// function createXLSX(payload){
//   // Create a new instance of a Workbook class
//   var wb = new xl.Workbook();
//   var fileName = ""
//
//   // Add Worksheets to the workbook
//   var wsUpload = wb.addWorksheet("Upload");
//   var wsDownload = wb.addWorksheet("Download");
//
//   for (var i = 0; i < payload.downloadTimeArr.length; i++) {
//     //Converts the current times to seconds
//     var currentUploadTime = payload.uploadTimeArr[i][0] + (payload.uploadTimeArr[i][1]/1000000000)
//     var currentDownloadTime = payload.downloadTimeArr[i][0] + (payload.downloadTimeArr[i][1]/1000000000)
//
//     wsUpload.cell(i+1,1).number(currentUploadTime*1000)
//     wsDownload.cell(i+1,1).number(currentDownloadTime*1000)
//   }
//
//   if(payload.region){
//     fileName += payload.region
//   }
//   fileName += payload.cloud
//   wb.write("./data/"+fileName+new Date()+".xlsx")
// }

/**
 * Function that prints out all of the help text needed to understand the tests
 *
 */
function showHelpText(){
  console.log("\n\nTo start a test, you have to write the following: \n\nnode index <number>\n\n")
  console.table("TEST NUMBERS W/ DESCRIPTION",[
  {
    number: 1,
    "test description": "Uploads and downloads a big file to/from Azure (EU-West)"
  }, {
    number: 2,
    "test description": "Uploads and downloads a big file to/from Storj.io"
  }, {
    number: 3,
    "test description": "Uploads and downloads a small file to/from Azure (EU-West)"
  }, {
    number: 4,
    "test description": "Uploads and downloads a small file to/from Storj.io"
  }, {
    number: 5,
    "test description": "Uploads and downloads a big file to/from Azure (US-West)"
  }, {
    number: 6,
    "test description": "Uploads and downloads a small file to/from Azure (US-West)"
  }
]);
}
